import discord
import os
from dotenv import load_dotenv
from discord.ext import commands
from discord.utils import get
from discord import FFmpegPCMAudio
from discord import TextChannel
from yt_dlp import YoutubeDL
import string, json
import aiohttp

load_dotenv()
bot = commands.Bot(command_prefix='*', intents=discord.Intents.all())

players = {}


@bot.event
async def on_ready():
    print('Bot online')


@bot.event
async def on_member_join(member):
    await member.send('Hello there! type %info in chat.')
    for ch in bot.get_guild(member.guild.id).channels:
        if ch.name == 'служебные-сообщения':
            await bot.get_channel(ch.id).send(f'{member.mention} joined us.')


@bot.command()
async def test(ctx):
    await ctx.send('im here')

@bot.command()
async def info(ctx, *, arg=None):
    author = ctx.message.author
    if arg == None:
        await ctx.send(f'{author.mention}\nInput next:\n*info clear\n*info join\n*info pause\n*info play\n*info resume\n*info stop\n*info test')
    elif arg == 'clear':
        await ctx.send(f'{author.mention}\nClearing messages in chat.')
    elif arg == 'join':
        await ctx.send(f'{author.mention}\nJoining into voice channel')
    elif arg == 'pause':
        await ctx.send(f'{author.mention}\nPausing audio')
    elif arg == 'play':
        await ctx.send(f'{author.mention}\nStart playing audio. example: "*play https://youtu.be/dQw4w9WgXcQ"')
    elif arg == 'resume':
        await ctx.send(f'{author.mention}\nContinue to play paused music')
    elif arg == 'stop':
        await ctx.send(f'{author.mention}\nStop playing music')
    else:
        await ctx.send(f'{author.mention}\nThere is no such command')

@bot.event
async def on_message(message):
    if {i.lower().translate(str.maketrans('','', string.punctuation)) for i in message.content.split(' ')}.intersection(set(json.load(open('cens.json')))) != set():
        await message.channel.send(f'{message.author.mention}, We don`t do that there')
        await message.delete()
    await bot.process_commands(message)
    if message.content.lower() == 'кот':
        async with aiohttp.ClientSession() as session:
            async with session.get('https://meow.senither.com/v1/random') as resp:
                data = await resp.json()
        if data['status'] == 200:
            await message.channel.send(data['data']['url'])

@bot.command()
async def join(ctx):
    channel = ctx.message.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)
    if voice and voice.is_connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()


@bot.command()
async def play(ctx, url):
    YDL_OPTIONS = {'format': 'bestaudio', 'noplaylist': 'True'}
    FFMPEG_OPTIONS = {
        'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}
    voice = get(bot.voice_clients, guild=ctx.guild)

    if not voice.is_playing():
        with YoutubeDL(YDL_OPTIONS) as ydl:
            info = ydl.extract_info(url, download=False)
        URL = info['url']
        voice.play(discord.FFmpegPCMAudio(executable="F:\\Discord-Bot-master\\FFmpeg\\bin\\ffmpeg.exe", source=URL,**FFMPEG_OPTIONS))
        voice.is_playing()
        await ctx.send('Bot is playing')
    else:
        await ctx.send("Bot is already playing")
        return


@bot.command()
async def resume(ctx):
    voice = get(bot.voice_clients, guild=ctx.guild)

    if not voice.is_playing():
        voice.resume()
        await ctx.send('Bot is resuming')


@bot.command()
async def pause(ctx):
    voice = get(bot.voice_clients, guild=ctx.guild)

    if voice.is_playing():
        voice.pause()
        await ctx.send('Bot has been paused')


@bot.command()
async def stop(ctx):
    voice = get(bot.voice_clients, guild=ctx.guild)
    if voice.is_playing():
        voice.stop()
        await ctx.send('Stopping...')


@bot.command()
async def clear(ctx, amount=5):
    await ctx.channel.purge(limit=amount)
    await ctx.send("Messages have been cleared")


bot.run(os.getenv('TOKEN'))
